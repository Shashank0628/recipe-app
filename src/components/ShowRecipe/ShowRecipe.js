import React ,  {useState , useEffect} from 'react';
import style from './ShowRecipe.module.css';
import CircularProgress from '@material-ui/core/CircularProgress';

const ShowRecipe = ({match})=> { 

    // *ID Parameter , API Key and States for this component
    const id = match.params.id
    const API_KEY = process.env.REACT_APP_API_KEY;
    const [recipeDescription , setRecipeDescription] = useState({})
    const [loading , setLoading] = useState(false)

    // *Callback function to fetch specific recipe from the API according to id parameter
    const fetchRecipe = async () => {
        setLoading(true)
        const response = await fetch(`https://api.spoonacular.com/recipes/${id}/information?apiKey=${API_KEY}`)
        const data = await response.json()
        setRecipeDescription(data)
        setLoading(false)
    }

    // *React Method to run when component is rendered for the first time
    useEffect(() => {
        fetchRecipe()   
    } , [])

    return ( <div className = {style.main}>
             <h1>Here Is Your Recipe</h1>
                {!loading ? (  <div className = {style.container}>
                                    <img src={recipeDescription.image} alt=""/>
                                    <div className={style.instruction} dangerouslySetInnerHTML={{__html: recipeDescription.instructions}}>
                                        
                                    </div>
                                    <div className = {style.description}>
                                        <h2>{recipeDescription.title}</h2>
                                        <p dangerouslySetInnerHTML={{__html: recipeDescription.summary}}></p>
                                    </div>
                                </div> ) : <CircularProgress /> }
             </div> )
}

export default ShowRecipe;