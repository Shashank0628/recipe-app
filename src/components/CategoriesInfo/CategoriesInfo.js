import React , {useState , useEffect , useContext} from 'react';
import Recipe from '../MainSection/Recipe/Recipe';
import style from './CategoriesInfo.module.css';
import CircularProgress from '@material-ui/core/CircularProgress';

const CategoriesInfo = ({match}) => {

    // *API Key and States for this component
    const API_KEY = process.env.REACT_APP_API_KEY;
    const cuisine = match.params.cuisine
    const [catRecipes , setCatRecipes] = useState([])
    const [loading , setLoading] = useState(false)

    // *Callback function to fetch specific recipes from the API according to cuisine parameter
    const fetchRecipes = async() => {
        setLoading(true)
        const response = await fetch(`https://api.spoonacular.com/recipes/complexSearch?cuisine=${cuisine}&apiKey=${API_KEY}`)
        const data = await response.json()
        setCatRecipes(data.results)
        setLoading(false)
    }

    // *React Method to run when component is rendered for the first time
    useEffect(()=> {
        fetchRecipes()
    } , [])

    return (
        <div className = {style.catInfo}>
            <h1>Here Are The Prides Of Cuisines</h1>
            { !loading ? (  <div className = {style.catContainer}>
                            {
                                catRecipes.map(
                                    recipe => (
                                        <div className = {style.item} key = {recipe.id}>
                                            <Recipe title = {recipe.title} image = {recipe.image} id = {recipe.id}></Recipe>
                                        </div>
                                    )
                                )
                            }
                          </div> ) : <CircularProgress /> }
        </div>
    )
}

export default CategoriesInfo


