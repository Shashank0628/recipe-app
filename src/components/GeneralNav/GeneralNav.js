import React from 'react';
import style from './GeneralNav.module.css';
import {Link , useHistory} from 'react-router-dom';
import {useAuth } from '../../AuthContext';

function GeneralNav() {
    const {currentUser , logout} = useAuth()
    const history = useHistory()

    const loggingOut = async (e) => {
        try{
            await logout()
            history.push('/')
        }
        catch(err){
            console.log(err)
        }
    }

    return (
        <nav className = {style.nav}>
            <div className={style.navItems}>
                <Link to="/" className = {style.navLinks}>Home</Link>
            </div>
            <div className={style.navItems}>
                <Link to="/" className = {style.navLinks}>About</Link>
            </div>
            {   !currentUser && (   <div className={style.navItems}>
                                        <Link to="/signin" className = {style.navLinks}>Sign In</Link>
                                    </div>  ) }
            { !currentUser && (     <div className={style.navItems}>
                                        <Link to="/signup" className = {style.navLinks}>Sign Up</Link>
                                    </div>  ) }
            { currentUser && (     <div className={style.navItems}>
                                        <Link to="/dashboard" className = {style.navLinks}>Dashboard</Link>
                                    </div>  ) }                        
            { currentUser && (      <div className={style.navItems}>
                                        <button onClick = {loggingOut} className = {style.navLinks}>Logout</button>
                                    </div>  ) }
        </nav>
    )
}
 
export default GeneralNav
