import React , {useState , useContext} from 'react';
import style from '../MainSection.module.css';
import {Link} from 'react-router-dom';


const Recipe = ({title , image , id}) => {
    const path = `/recipes/${id}`
    return (
        <div className = {style.item}> 
            <h2>{title}</h2>
            <img src={image} alt="item-image"/>
            <a href = {path}> Show Recipe </a>
        </div>
    )


}

export default Recipe;