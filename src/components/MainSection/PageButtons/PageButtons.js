import React , {useState , useEffect , useContext} from 'react';
import {useCurrentPage} from '../../../CurrentPage';
import {usePageSize} from '../../../PageSize';
import {useRecipe} from '../../../RecipeContext';
import style from './PageButtons.module.css';

const PageButtons = () => {
    
    // *States for this component
    const [currentPage , setCurrentPage] = useCurrentPage()
    const [pageSize , setPageSize] = usePageSize()
    const {recipes , setRecipes} = useRecipe()
    const [pages , setPages] = useState([])
    
    // *Temporary array to keep track of buttons 
    let arr = []

    // *Callback to setup page buttons according to recipes length and page size
    const setupPagination = () => {
      let no_of_pages = Math.ceil(recipes.length/pageSize)
      for(let i = 1 ; i<= no_of_pages ; i++){
          arr.push(i)
      }
      setPages(arr)
    }

    // *React Method to run everytime component is rendered due to recipes state
    useEffect(() => {
        setupPagination()
    } , [recipes])

    return (
        <div className={style.pageContainer}>
            {
                pages.map(page => {
                    if(page==currentPage){
                        return ( 
                        <button 
                        onClick = { function(e){
                            setCurrentPage(page)
                        } }
                        className = {style.pageButtons + " " + style.active} key={page}>{page}</button>
                        )
                        
                    }
                    else{
                        return ( 
                            <button 
                            onClick = {function(e){
                                setCurrentPage(page)
                            }}
                            className = {style.pageButtons} key={page}>{page}</button>
                        )
                    }
                })
            }
        </div>
    )
}

export default PageButtons;