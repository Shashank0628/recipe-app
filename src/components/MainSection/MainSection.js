import React , {useState , useContext, useEffect} from 'react';
import {useRecipe} from '../../RecipeContext';
import style from './MainSection.module.css';
import Recipe from './Recipe/Recipe';
import PageButtons from './PageButtons/PageButtons';
import {usePaginatedRecipe} from '../../PaginatedRecipeContext';
import {useCurrentPage} from '../../CurrentPage';
import {usePageSize} from '../../PageSize';
import CircularProgress from '@material-ui/core/CircularProgress';

const MainSection = () => {

    // *Original Recipe State
    const {recipes, setRecipes , loading} = useRecipe()
    
    // *For Pagination 

    const [paginatedRecipes , setPaginatedRecipes] = usePaginatedRecipe()
    const [currentPage , setCurrentPage] = useCurrentPage()
    const [pageSize , setPageSize] = usePageSize()

    // *Callback to filter recipes according to current page and page size for pagination
    const sliceItems = () => {
        let page_number = currentPage
        let rows_per_items = pageSize
        page_number--
        let start_loop = page_number*rows_per_items
        let end_loop = start_loop + rows_per_items
        setPaginatedRecipes(recipes.slice(start_loop , end_loop))
    }

    // *React Method to run everytime component is rendered due to recipes state or current page state
    useEffect( () => {
        sliceItems()
    } , [recipes , currentPage])

    return (
        <div className=  {style.container} id='mainSectionContainer'>
            <h1>Here are some of the top picks for you</h1>
            { !loading ?
               (<div>
                    <div className = {style.itemContainer}>
                        { paginatedRecipes.map(recipe => (
                            <Recipe title = {recipe.title} image = {recipe.image} id = {recipe.id} key = {recipe.id}/>
                        )) }
                    </div>
                    <PageButtons />
                </div>) : <CircularProgress />
            }
        </div>
    )
}

export default MainSection;