import React from 'react';
import style from './HistorySection.module.css';

const HistorySection = () => {
    return (
        <div className = {style.history}>
            <div className = {style.bgImage}></div>
            <div className = {style.mainContent}>
                <h1>Discover</h1>
                <h2>Our History</h2>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, odio dolorem maiores blanditiis mollitia unde harum sit eum facere, totam quaerat commodi odit. Labore, voluptates culpa minus reiciendis assumenda delectus, nostrum ducimus commodi quia eum laudantium, reprehenderit voluptatum rem sint. Possimus eveniet id esse ipsum. Est a accusantium voluptates laboriosam! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cupiditate reprehenderit unde eaque recusandae ratione sapiente, eum quis, aut asperiores et nihil suscipit sint quo. Maxime dolor aliquam eius provident quod velit sequi saepe natus unde optio architecto repudiandae, quibusdam alias.</p>
                <a href="#">Show More</a>
            </div>
        </div>
    )
}

export default HistorySection;