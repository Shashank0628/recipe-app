import React, {useState , useEffect, useContext} from 'react';
import Navbar from './Navbar/Navbar';
import Content from './Content/Content';
import style from './Banner.module.css';

const Banner = ()=>{
    return (
        <div className = {style.banner}>
            <div>
                <img src='./logo3.png' alt="" id = {style.logo} />
            </div>
            <Navbar />
            <Content />
        </div>

    )
}

export default Banner;