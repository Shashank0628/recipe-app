import React , {useState , useEffect , useContext} from 'react';
import {useRecipe} from '../../../RecipeContext';
import style from './Content.module.css';

const Content = () => {

    // *API Key and States for this component
    const API_KEY = process.env.REACT_APP_API_KEY;
    const [search , setSearch] = useState('')
    const [query  , setQuery] = useState('')
    const {recipes,setRecipes , loading , setLoading} = useRecipe()

    // *Callback function to fetch recipes from the API
    const getRecipe = async () => {
        setLoading(true)
        const response = await fetch(`https://api.spoonacular.com/recipes/complexSearch?query=${query}&apiKey=${API_KEY}`)
        const data = await response.json()
        setRecipes(data.results)
        setLoading(false)
    }

    // *Callback to set the search state equal to input value
    const getSearch = e => {
        setSearch(e.target.value)
    }

    // *Callback to set query state equal to search state on form submit
    const onSubmit = e => {
        e.preventDefault()
        setQuery(search)
        setSearch('')
    }

    // *Callback to scroll down on button click
    const buttonClick = e => {
        const mainSection = document.getElementById('mainSectionContainer')
        mainSection.scrollIntoView()
    }
    
    // *React Method to run everytime component is rendered due to query state
    useEffect(() => { 
        getRecipe()
    } , [query])

    return (
       <div className = {style.content}>
           <h1> Eatin’ Good in Our Land </h1>
           <h3>Come and Experience Our Best Recipes</h3>

           <form action="" className = {style.form} onSubmit = {onSubmit}>
               <input type="text" placeholder = 'Search For Recipes' value = {search} onChange = {getSearch} />
               <button onClick = {buttonClick}>Search</button>
           </form>
       </div>
    )
}

export default Content