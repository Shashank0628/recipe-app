import React , {useContext} from 'react';
import style from './Navbar.module.css';
import {Link } from 'react-router-dom';
import {useAuth} from '../../../AuthContext';

const Navbar = () => {
    const {currentUser , logout} = useAuth()

    const loggingOut = async (e) => {
        try{
            await logout()
        }
        catch(err){
            console.log(err)
        }
    }

    return (
        <nav>
            <ul className = {style.navUL}>
                <li><a href='#'> Home </a></li>
                <li><a href='#mainSectionContainer'>Recipes</a></li>
               { !currentUser && <li><Link to='/signin'>Signin </Link></li> } 
               { !currentUser && <li><Link to='/signup'>Signup</Link></li> }
               { currentUser && <li><Link to='/dashboard'>Dashboard</Link></li>}
               { currentUser && <li><button onClick = {loggingOut}>Logout</button></li> }
            </ul>
        </nav>
    )
}

export default Navbar