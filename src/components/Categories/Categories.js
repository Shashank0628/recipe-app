import React from 'react';
import style from './Categories.module.css';
import {Link} from 'react-router-dom';

const Categories = () => {
    return (
        <div className = {style.main}>
            <h1>Explore The Food Beyond The Boundaries</h1>
            <div className = {style.container}>

                <div className = {style.item}>
                    <img src="./Indian.jpg" alt="Indian-Cuisine"/>
                    <Link to = '/recipes/categories/Indian' className = {style.link}>Indian</Link>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, ex vero. Dolores quaerat tempora, in esse fugit hic quos consectetur!</p>
                </div>

                <div className = {style.item}>
                    <img src="./Italian.jpg" alt="Italian-Cuisine"/>
                    <Link to = '/recipes/categories/Italian' className = {style.link}>Italian</Link>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, ex vero. Dolores quaerat tempora, in esse fugit hic quos consectetur!</p>
                </div>
                
                <div className = {style.item}>
                    <img src="./French.jpg" alt="French-Cuisine"/>
                    <Link to = '/recipes/categories/French' className = {style.link}>French</Link>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo, ex vero. Dolores quaerat tempora, in esse fugit hic quos consectetur!</p>
                </div>
                
            </div>
        </div>
    )
}

export default Categories