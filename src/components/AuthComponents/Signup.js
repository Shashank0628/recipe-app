import React , {useState , useRef} from 'react'
import style from './AuthStyles.module.css';
import GeneralNav from '../GeneralNav/GeneralNav';
import {Card , Alert, Button} from 'react-bootstrap';
import {useAuth} from '../../AuthContext';
import {useHistory} from 'react-router-dom';

const Signup = () => {
    const emailRef = useRef()
    const passwordRef = useRef()
    const confirmPasswordRef = useRef()
    const [error , setError] = useState("")
    const [loading , setLoading] = useState(false)
    const {signup} = useAuth()
    const history = useHistory()
    
    const handleSubmit = async (e) => {
        e.preventDefault()
        setError("")
        setLoading(true)

        if(passwordRef.current.value !== confirmPasswordRef.current.value){
             setError("Password does not match")
             setLoading(false)
             passwordRef.current.value = ""
             confirmPasswordRef.current.value = ""
             return
        }

        try{
            await signup(emailRef.current.value , passwordRef.current.value)
            history.push('/')

        }catch(err){
            setError(err.message)
            setLoading(false)
            emailRef.current.value = ""
            passwordRef.current.value = ""
            confirmPasswordRef.current.value = ""
        }
    }

    return (
        <div className = {style.main}>
            <GeneralNav />
            <div className = {style.mainContainer}>
                <Card className = "w-50">
                    <Card.Body>
                        <h3 className='text-center mt-10 mb-5' >This is Signup Page</h3>
                        <form onSubmit = {handleSubmit}>
                            {error && <Alert variant = "danger">{error}</Alert>}
                            <p className = {style.formGroup}>
                                <label htmlFor="email">Email: </label>
                                <input type="email" placeholder = "Email Address" ref = {emailRef} required/>
                            </p>

                            <p className = {style.formGroup}>
                                <label htmlFor="password">Password: </label>
                                <input type="password" placeholder = "Password" ref = {passwordRef} required/>
                            </p>

                            <p className = {style.formGroup}>
                                <label htmlFor="confirmPassword">Comfirm Password: </label>
                                <input type="password" placeholder = "Confirm Password" ref = {confirmPasswordRef} required/>
                            </p>

                            <p className = {style.formGroup}>
                                <Button className = 'w-100 mt-3' type = 'submit' disabled = {loading}>
                                    Signup
                                </Button>
                            </p>

                        </form>
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}

export default Signup
