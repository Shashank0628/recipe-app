import React from 'react'
import style from './Footer.module.css';

function Footer() {
    return (
        <div className = {style.footer}>

            <div className={style.footerItems}>
                <h1><span>Our</span> Location</h1>
                <p>Dummy Text 1</p>
                <p>Dummy Text 2</p>
                <p>Dummy Text 3</p>
            </div>

            <div className={style.footerItems}>
                <h1><span>Our</span> Location</h1>
                <p>Dummy Text 1</p>
                <p>Dummy Text 2</p>
                <p>Dummy Text 3</p>
            </div>

            <div className={style.footerItems}>
                <h1><span>Api's</span> Used</h1>
                <p>https://spoonacular.com</p>
            </div>
            
        </div>
    )
}

export default Footer
