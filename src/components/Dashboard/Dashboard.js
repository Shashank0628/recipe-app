import React from 'react'
import GeneralNav from '../GeneralNav/GeneralNav';
import style from './Dashboard.module.css';
import {Card , Button} from 'react-bootstrap';
import {useAuth} from '../../AuthContext';
import {Link} from 'react-router-dom';

const Dashboard = () => {
    const {currentUser} = useAuth() 
    return (
        <div>
            <GeneralNav />
            <h2 style = {{paddingTop: "30px"}}>This is User Dashboard</h2>
            <div className = {style.main}>
                <Card>
                    <Card.Body>
                        <h6><strong>Email: </strong> {currentUser.email}</h6>
                    </Card.Body>
                </Card>
            </div>   
        </div>
    )
}

export default Dashboard
