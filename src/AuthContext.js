import React , {useState , createContext , useContext , useEffect} from 'react';
import {auth} from './firebase'

const AuthContext = createContext()

export const useAuth = () => useContext(AuthContext)

export const AuthProvider = ({children}) => {

    const [currentUser , setCurrentUser] = useState()
    const [loading , setLoading] = useState(true)
    
    useEffect(() => {
        const unsubscriber = auth.onAuthStateChanged(user => {
            setCurrentUser(user)
            setLoading(false)
        })
        return unsubscriber
    } , [])

    const login = (email , password) => auth.signInWithEmailAndPassword(email , password)
    const signup = (email , password) => auth.createUserWithEmailAndPassword(email , password)
    const logout = () => auth.signOut()

    return (
        <AuthContext.Provider value = {{currentUser , login , signup , logout}}>
            {!loading && children}
        </AuthContext.Provider>
    )
}

