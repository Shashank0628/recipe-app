import React , {useState , createContext, useContext} from 'react';

const CurrentPageContext = createContext()

export const useCurrentPage = () => useContext(CurrentPageContext)

export const CurrentPageProvider = (props) => {
    const [currentPage , setCurrentPage] = useState(1)

    return (
        <CurrentPageContext.Provider value = {[currentPage, setCurrentPage]}>
            {props.children}
        </CurrentPageContext.Provider>
    )
}