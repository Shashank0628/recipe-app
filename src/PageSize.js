import React , {useState , createContext, useContext} from 'react';

const PageSizeContext = createContext()

export const usePageSize = () => useContext(PageSizeContext)

export const PageSizeProvider = (props) => {
    const [pageSize , setPageSize] = useState(2)

    return (
        <PageSizeContext.Provider value = {[pageSize, setPageSize]}>
            {props.children}
        </PageSizeContext.Provider>
    )
}