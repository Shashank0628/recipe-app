import React , {useState , createContext, useContext} from 'react';

const PaginatedRecipeContext = createContext()

export const usePaginatedRecipe = () => useContext(PaginatedRecipeContext)

export const PaginatedRecipeProvider = (props) => {
    const [paginatedRecipes , setPaginatedRecipes] = useState([])

    return (
        <PaginatedRecipeContext.Provider value = {[paginatedRecipes, setPaginatedRecipes]}>
            {props.children}
        </PaginatedRecipeContext.Provider>
    )
}