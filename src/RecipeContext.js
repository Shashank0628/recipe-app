import React , {useState , createContext , useContext} from 'react';

const RecipeContext = createContext()

export const useRecipe = () => useContext(RecipeContext)

export const RecipeProvider = (props) => {
    const [recipes , setRecipes] = useState([])
    const [loading , setLoading] = useState(false)

    return (
        <RecipeContext.Provider value = {{recipes , setRecipes , loading , setLoading}}>
            {props.children}
        </RecipeContext.Provider>
    )
}