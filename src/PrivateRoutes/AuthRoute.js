import React from 'react'
import {useAuth} from '../AuthContext';
import {Route , Redirect} from 'react-router-dom';

const AuthRoute = ({component: Component , ...rest}) => {
    const {currentUser} = useAuth()
    return (
        <Route {...rest} render = {props => currentUser ? <Component /> : <Redirect to ='/signin' />}/>
    )
}

export default AuthRoute
