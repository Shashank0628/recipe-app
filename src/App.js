import React from 'react';
import './App.css';
import {BrowserRouter as Router , Route , Switch} from 'react-router-dom';
import AuthRoute from './PrivateRoutes/AuthRoute';

// *Requiring Components
import Banner from './components/Banner/Banner';
import MainSection from './components/MainSection/MainSection';
import HistorySection from './components/HistorySection/HistorySection';
import GeneralNav from './components/GeneralNav/GeneralNav';
import ShowRecipe from './components/ShowRecipe/ShowRecipe';
import Dashboard from './components/Dashboard/Dashboard';
import Categories from './components/Categories/Categories';
import CategoriesInfo from './components/CategoriesInfo/CategoriesInfo';
import Signin from './components/AuthComponents/Signin';
import Signup from './components/AuthComponents/Signup';
import Footer from './components/Footer/Footer';

// *Requiring Context Providers
import {RecipeProvider} from './RecipeContext';
import {PaginatedRecipeProvider} from './PaginatedRecipeContext';
import {CurrentPageProvider} from './CurrentPage';
import {PageSizeProvider} from './PageSize';
import {AuthProvider} from './AuthContext';

function App() {
  return (  
          <RecipeProvider>
            <PaginatedRecipeProvider>
                  <CurrentPageProvider>
                    <PageSizeProvider>
                      <AuthProvider>
                        <Router>
                            <div className="App">
                              <Route path='/' component={Banner} exact/>
                              <Route path='/' component={MainSection} exact/>
                              <Route path='/' component={HistorySection} exact/>
                              <Route path='/' component={Categories} exact/>
                              <Route path='/recipes/:id' component = {GeneralNav} />
                              <Route path='/recipes/:id' component = {ShowRecipe} exact/>
                              <Route path='/recipes/categories/:cuisine' component = {CategoriesInfo} exact/>
                              <Route path = '/signin' component = {Signin} />
                              <Route path = '/signup' component = {Signup} />
                              <AuthRoute path = '/dashboard' component = {Dashboard} />
                              <Footer />
                            </div>
                        </Router>
                      </AuthProvider>
                    </PageSizeProvider>
                  </CurrentPageProvider>
            </PaginatedRecipeProvider>
          </RecipeProvider> 
         );
}

export default App;
